"use strict";

let stored = {};
const digits = [...Array(10).keys()].map((key) => key.toString());

const operations = {
    "+": (first, second) => first + second,
    "-": (first, second) => first - second,
    "*": (first, second) => first * second,
    "/": (first, second) => first / second
};

const elements = {
    get display() {
        return document.getElementById("display");
    },
    get formula() {
        return document.getElementById("formula");
    },
    updateFormula(val) {
        document.getElementById("formula").textContent += val;
    },
    digitButtons: (() => {
        const buttons = {};
        for (let digit of digits)
            Object.defineProperty(buttons, digit, {
                enumerable: true,
                get: () => document.getElementById(`btn-${digit}`)
            });
        return buttons;
    })(),
    get separatorButton() {
        return document.getElementById("btn-separator");
    },
    get clearButton() {
        return document.getElementById("btn-clear");
    },
    operationButtons: (() => {
        const buttons = {};
        for (let opCode of Object.keys(operations))
            Object.defineProperty(buttons, opCode, {
                enumerable: true,
                get: () => document.getElementById({
                    "+": "btn-add",
                    "-": "btn-subtract",
                    "*": "btn-multiply",
                    "/": "btn-divide"
                }[opCode])
            });
        return buttons;
    })(),
    get calculateButton() {
        return document.getElementById("btn-calculate");
    }
}

function setUpEntryButtons() {
    for (let [digit, button] of Object.entries(elements.digitButtons))
        button.addEventListener("click", function () {
            elements.display.textContent += digit;
            stored.lastInputType = "digit";
            elements.updateFormula(digit);
        });

    elements.separatorButton.addEventListener("click", function () {
        const text = elements.display.textContent;
        if (text.length && text.indexOf(".") === -1) {
            elements.display.textContent += ".";
            elements.updateFormula(".");
        }
    });

    elements.clearButton.addEventListener("click", function () {
        elements.display.textContent = "";
        stored = {};
        elements.formula.textContent = "";
    });
}

function calculate() {
    const [first, second] = [stored.text, stored.lastNumber]
        .map((text) => parseFloat(text));
    return operations[stored.opCode](first, second);
}

function setUpOperationButtons() {
    for (let [opCode, button] of Object.entries(elements.operationButtons))
        button.addEventListener("click", function () {

            elements.updateFormula(opCode);

            // new number will be a negative number
            if (stored.lastInputType == "sign" && opCode == "-") {
                elements.display.textContent = `-${elements.display.textContent}`;
                return;
            }

            if (stored.lastInputType != "equal") {

                stored.lastNumber = stored.text ? elements.display.textContent : null;
                stored.text = stored.text ? calculate() : elements.display.textContent;
                stored.opCode = opCode;
                // this is a bug and it should be outside the if, but from my understanding
                // of the the requirement, I'm not allowed to change it at this point 
                stored.lastInputType = "sign";
            }
            else {   // make sure we can stil process `-` operations after `=` was pressed
                stored.opCode = opCode;
            }
            elements.display.textContent = "";
        });
}

function setUpCalculateButton() {
    elements.calculateButton.addEventListener("click", function () {
        if (!stored)
            return;
        // save the current number, so can apply the same operation again, if we need to
        if (stored.lastInputType == "digit")
            stored.lastNumber = elements.display.textContent;
        else {
            // update the formula to show what operation was applied
            elements.updateFormula(stored.opCode + stored.lastNumber)
        }

        stored.lastInputType = "equal";
        elements.display.textContent = calculate();
        stored.text = elements.display.textContent;
    });
}

(() => {
    setUpEntryButtons();
    setUpOperationButtons();
    setUpCalculateButton();
})();
